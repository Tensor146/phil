FROM golang:alpine as builder

ENV GOPATH=/srv/

RUN apk add --update git make gcc openssh

RUN go get -u github.com/golang/dep/cmd/dep

WORKDIR /srv/src/bitbucket.org/Tensor146/phil/app

COPY ./app/Gopkg.lock ./app/Gopkg.toml /srv/src/bitbucket.org/Tensor146/phil/app/

RUN /srv/bin/dep ensure --vendor-only

COPY ./app/ /srv/src/bitbucket.org/Tensor146/phil/app

RUN go build -o main

FROM alpine:3.7

RUN apk add --update bash ca-certificates curl

WORKDIR /srv/app/

COPY --from=builder /srv/src/bitbucket.org/Tensor146/phil/app/main /srv/app

RUN chmod +x /srv/app/main

ENTRYPOINT /srv/app/main serve
