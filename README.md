# Phil

Phil is a simple file storage service, written on Golang.

## Features
  - Basic auth;
  - Redis as file metadata storage;
  - Embed event bus;
  - DI-based project structure;
  - See /api for api usage examples (via postman);
  - See /build/package for docker-compose file to run phil easily.

# Description
## Usage
   phil [global options] command [command options] [arguments...]

## Version
   0.1.10

## Commands
     serve    Raises phil daemon
     help, h  Shows a list of commands or help for one command

## Global options
   - --app-dir value           Application work directory (default: "%current") [$PHIL_APP_DIR]
   - --log-level value         Logging level (default: "info") [$PHIL_LOG_LEVEL]
   - --help, -h                show help
   - --version, -v             print the version

# Serve command
## Usage
   phil serve [command options] [arguments...]

## Options
   - --http-host value         Http host address (default: "127.0.0.1") [$PHIL_HTTP_HOST]
   - --http-port value         Http port (default: 3000) [$PHIL_HTTP_PORT]
   - --http-username value     Http auth access check: username [$PHIL_HTTP_USERNAME]
   - --http-password value     Http auth access check: password [$PHIL_HTTP_PASSWORD]
   - --redis-host value        Redis host address (default: "127.0.0.1") [$PHIL_REDIS_HOST]
   - --redis-port value        Redis port (default: 6379) [$PHIL_REDIS_PORT]
   - --redis-password value    Redis password [$PHIL_REDIS_PASSWORD]
   - --redis-db value          Redis database number (0-15) (default: 0) [$PHIL_REDIS_DB]
   - --file-storage-dir value  Dir to store saved files (default: "/tmp/phil/files") [$PHIL_FILE_STORAGE_DIR]
