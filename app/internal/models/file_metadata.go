package models

import (
	"strconv"
	"time"

	"github.com/pkg/errors"
)

type FileMetadata struct {
	Name             string
	Hash             string
	Size             int64
	FilePath         string
	ContentType      string
	CountOfDownloads uint64
	CreatedAt        time.Time
	UpdatedAt        time.Time
}

func NewFileMetadata(name string, hash string, filePath string, size int64, contentType string) FileMetadata {
	return FileMetadata{
		Name:             name,
		Hash:             hash,
		FilePath:         filePath,
		Size:             size,
		ContentType:      contentType,
		CountOfDownloads: 0,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
}

type RMap map[string]string

func UnmarshalFileMetadataFromRmap(rmap RMap, fileMd *FileMetadata) (err error) {
	if fileMd == nil {
		return errors.New("Can not unmarshal data into nil")
	}
	var key string
	var value string
	defer func() {
		if err != nil {
			err = errors.Wrapf(err, "error parsing field \"%s\"", key)
		}
	}()
	for key, value = range rmap {
		switch key {
		case "name":
			fileMd.Name = value
		case "hash":
			fileMd.Hash = value
		case "size":
			fileMd.Size, err = strconv.ParseInt(value, 10, 64)
			if err != nil {
				return
			}
		case "file_path":
			fileMd.FilePath = value
		case "content_type":
			fileMd.ContentType = value
		case "count_of_downloads":
			fileMd.CountOfDownloads, err = strconv.ParseUint(value, 10, 64)
			if err != nil {
				return
			}
		case "created_at":
			var unixTime int64
			unixTime, err = strconv.ParseInt(value, 10, 64)
			if err != nil {
				return
			}
			fileMd.CreatedAt = time.Unix(unixTime, 0)
		case "updated_at":
			var unixTime int64
			unixTime, err = strconv.ParseInt(value, 10, 64)
			if err != nil {
				return
			}
			fileMd.UpdatedAt = time.Unix(unixTime, 0)
		}
	}

	return
}

func MarshalFileMetadataIntoRmap(fileMd FileMetadata) (rmap *RMap, err error) {
	return &RMap{
		"name":               fileMd.Name,
		"hash":               fileMd.Hash,
		"size":               strconv.FormatInt(fileMd.Size, 10),
		"file_path":          fileMd.FilePath,
		"content_type":       fileMd.ContentType,
		"count_of_downloads": strconv.FormatUint(fileMd.CountOfDownloads, 10),
		"created_at":         strconv.FormatInt(fileMd.CreatedAt.Unix(), 10),
		"updated_at":         strconv.FormatInt(fileMd.UpdatedAt.Unix(), 10),
	}, nil
}
