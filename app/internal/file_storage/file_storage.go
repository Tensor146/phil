package file_storage

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"io/ioutil"
	"path/filepath"
	"time"

	"github.com/asaskevich/EventBus"

	"bitbucket.org/Tensor146/phil/app/internal/config"
	"bitbucket.org/Tensor146/phil/app/internal/metadata_storage"
	"bitbucket.org/Tensor146/phil/app/internal/models"
	"bitbucket.org/Tensor146/phil/app/pkg/helpers"
)

var ErrFileNotFound = errors.New("File not found")

type FileStorage struct {
	bus EventBus.Bus
	ms  *metadata_storage.MetadataStorage
	log *logrus.Logger
	cfg *config.Config
}

func NewFileStorage(bus EventBus.Bus, ms *metadata_storage.MetadataStorage, log *logrus.Logger, cfg *config.Config) (*FileStorage, error) {
	if err := helpers.NewUnexistingDependencyError("bus", bus); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("metadata_storage", ms); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("cfg", cfg); err != nil {
		return nil, err
	}

	fs := &FileStorage{
		bus: bus,
		ms:  ms,
		log: log,
		cfg: cfg,
	}

	return fs, nil
}

func (fs *FileStorage) Serve() error {
	if fileInfo, err := os.Stat(fs.cfg.FileStorageDir); err == nil {
		if !fileInfo.IsDir() {
			return errors.Errorf("%s id not a directory", fs.cfg.FileStorageDir)
		}
	} else {
		if err := os.MkdirAll(fs.cfg.FileStorageDir, os.ModePerm); err != nil {
			return errors.Wrapf(err, "can not create directory: %s", fs.cfg.FileStorageDir)
		}
	}

	return nil
}

func (fs *FileStorage) SaveFile(fileContent io.Reader, fileName string, fileType string) (*models.FileMetadata, error) {
	// Unfortunately, we need to create temp file to perform multiple reads: hashing and writing (saving).
	tempFile, err := ioutil.TempFile("", "phil-tempfile")
	if err != nil {
		return nil, errors.Wrap(err, "temp file creation error")
	}

	var fileSize int64
	if fileSize, err = io.Copy(tempFile, fileContent); err != nil {
		return nil, errors.Wrap(err, "temp file writing error")
	}

	deleteTempFile := true
	defer func() {
		if deleteTempFile {
			if err := os.Remove(tempFile.Name()); err != nil {
				fs.log.Errorf("tempfile \"%s\" deletion error: %+v", tempFile.Name(), err)
			}
		}
	}()
	defer tempFile.Close()

	hash, err := fs.hashFile(tempFile)
	if err != nil {
		return nil, errors.Wrap(err, "file hashing error")
	}

	fileMd, err := fs.ms.GetByHash(hash)
	if err == metadata_storage.NoMetadataFoundError {
		filePath, err := fs.buildFilePath(hash)
		if err != nil {
			return nil, errors.Wrap(err, "file path building error")
		}
		if _, err := os.Stat(filePath); err == nil {
			// It means, that there necessary file already exists.
			return nil, errors.Errorf("file \"%s\" already exists", filePath)
		}

		dirPath := filepath.Dir(filePath)
		if err := os.MkdirAll(dirPath, os.ModePerm); err != nil {
			return nil, errors.Wrapf(err, "can not create directory: %s", dirPath)
		}
		tempFile2, err := os.Open(tempFile.Name())
		if err != nil {
			return nil, errors.Wrapf(err, "can not reopen temp file: %s", tempFile.Name())
		}
		defer tempFile2.Close()
		targetFile, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			return nil, errors.Wrapf(err, "can create target file: %s", filePath)
		}
		defer targetFile.Close()
		if _, err = io.Copy(targetFile, tempFile2); err != nil {
			return nil, errors.Wrapf(err, "file saving error: can not copy temp file %s->%s", tempFile2.Name(), targetFile.Name())
		}
		deleteTempFile = false

		fileMd := models.NewFileMetadata(fileName, hash, filePath, fileSize, fileType)
		if err = fs.ms.Save(fileMd); err != nil {
			return nil, errors.Wrapf(err, "can not save file metadata for hash \"%s\"", hash)
		}

		return &fileMd, nil
	} else if err != nil {
		return nil, errors.Wrapf(err, "can not get file metadata for hash \"%s\"", hash)
	} else if fileMd.Name != fileName || fileMd.ContentType != fileType {
		fileMd.UpdatedAt = time.Now()
		fileMd.Name = fileName
		fileMd.ContentType = fileType
		if err := fs.ms.Save(*fileMd); err != nil {
			return nil, errors.Wrapf(err, "can not updated file metadata for hash \"%s\"", hash)
		}
	}

	return fileMd, nil
}

func (fs *FileStorage) GetFileMd(hash string, incCounters bool) (*models.FileMetadata, error) {
	defer func() {
		// Deferred because it should be incremented even on error state.
		if incCounters {
			fs.IncDownloadsCount(hash)
		}
	}()
	fileMd, err := fs.ms.GetByHash(hash)
	if err != nil {
		if err == metadata_storage.NoMetadataFoundError {
			return nil, ErrFileNotFound
		}
		return nil, errors.Wrapf(err, "can not get file metadata for hash \"%s\"", hash)
	}

	if _, err := os.Stat(fileMd.FilePath); os.IsNotExist(err) {
		return nil, errors.Errorf("file \"%s\" (%s) does not exist", fileMd.FilePath, hash)
	}

	return fileMd, nil
}

func (fs *FileStorage) IncDownloadsCount(hash string) error {
	if err := fs.ms.IncDownloadsCount(hash); err != nil {
		return errors.Wrapf(err, "can not increment file downloads count for hash \"%s\"", hash)
	}

	return nil
}

func (fs *FileStorage) DeleteFile(hash string) error {
	fileMd, err := fs.ms.GetByHash(hash)
	if err != nil {
		if err == metadata_storage.NoMetadataFoundError {
			return ErrFileNotFound
		}
		return errors.Wrapf(err, "can not get file metadata for hash \"%s\"", hash)
	}
	if _, err := os.Stat(fileMd.FilePath); os.IsNotExist(err) {
		return errors.Errorf("file \"%s\" (%s) does not exist", fileMd.FilePath, hash)
	}
	if err = os.Remove(fileMd.FilePath); err != nil {
		return errors.Wrap(err, "file deletion error")
	}
	if err = fs.ms.DeleteByHash(hash); err != nil {
		return errors.Wrapf(err, "can not delete file metadata for hash \"%s\"", hash)
	}

	return nil
}

func (fs *FileStorage) hashFile(file io.Reader) (string, error) {
	hasher := sha256.New()
	_, err := io.Copy(hasher, file)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hasher.Sum(nil)), nil
}

func (fs *FileStorage) buildFilePath(hash string) (string, error) {
	if len(hash) < 2 {
		return "", errors.New("file hash is too short")
	}

	prefix := hash[:2]

	return filepath.Join(fs.cfg.FileStorageDir, prefix, hash), nil
}
