package http

import (
	"context"

	"github.com/labstack/echo"
	logrusMiddleware "github.com/neko-neko/echo-logrus"
	logrusWrapper "github.com/neko-neko/echo-logrus/log"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"fmt"
	"net/http"
	"strings"

	"github.com/asaskevich/EventBus"
	"github.com/labstack/echo/middleware"

	"bitbucket.org/Tensor146/phil/app/internal/config"
	"bitbucket.org/Tensor146/phil/app/internal/event_bus"
	"bitbucket.org/Tensor146/phil/app/internal/file_storage"
	"bitbucket.org/Tensor146/phil/app/pkg/helpers"
)

type HttpServer struct {
	fs  *file_storage.FileStorage
	e   *echo.Echo
	log *logrus.Logger
	cfg *config.Config
	bus EventBus.Bus
}

func NewHttpServer(fs *file_storage.FileStorage, bus EventBus.Bus, log *logrus.Logger, cfg *config.Config) (*HttpServer, error) {
	if err := helpers.NewUnexistingDependencyError("file_storage", fs); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("bus", bus); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("cfg", cfg); err != nil {
		return nil, err
	}

	h := &HttpServer{
		fs:  fs,
		bus: bus,
		e:   echo.New(),
		log: log,
		cfg: cfg,
	}

	h.configureEcho(h.e)

	return h, nil
}

func Serve(srv *HttpServer) error {
	srv.log.Debugf("http: listening %s:%d", srv.cfg.HttpHost, srv.cfg.HttpPort)
	if err := srv.e.Start(fmt.Sprintf("%s:%d", srv.cfg.HttpHost, srv.cfg.HttpPort)); err != nil {
		return errors.Wrap(err, "can not start http server")
	}

	return nil
}

func (srv *HttpServer) configureEcho(e *echo.Echo) {
	e.Logger = &logrusWrapper.MyLogger{
		Logger: srv.log,
	}
	e.HideBanner = true
	e.HidePort = true
	e.Use(func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
		return func(i echo.Context) error {
			srv.bus.Publish(event_bus.EventHttpRequest, event_bus.EvHttpRequest{})
			handlerFunc(i)
			srv.bus.Publish(event_bus.EventHttpResponse, event_bus.EvHttpResponse{})
			return nil
		}
	})
	// In future is better to replace it with non-global logger.
	e.Use(logrusMiddleware.Logger())
	if srv.cfg.HttpUser != "" || srv.cfg.HttpPassword != "" {
		e.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
			if username == srv.cfg.HttpUser && password == srv.cfg.HttpPassword {
				return true, nil
			}
			return false, nil
		}))
	}
	srv.registerRoutes(e)
}

func (srv *HttpServer) registerRoutes(e *echo.Echo) {
	e.POST("/upload", func(ctx echo.Context) error {
		formFile, err := ctx.FormFile("file")
		if err != nil {
			return setErrResponse(400, "missing param: \"file\"", ctx)
		}
		file, err := formFile.Open()
		if err != nil {
			return setErrResponse(400, "invalid param: \"file\" (reading error)", ctx)
		}
		defer file.Close()

		contentTypes, _ := formFile.Header["Content-Type"]
		fileMetadata, err := srv.fs.SaveFile(file, formFile.Filename, strings.Join(contentTypes, ";"))
		if err != nil {
			return errors.Wrap(err, "can not save \"file\"")
		}

		return ctx.JSON(http.StatusOK, fileMetadata)
	})
	e.GET("/file/:hash", func(ctx echo.Context) error {
		hash := ctx.Param("hash")
		if hash == "" {
			return setErrResponse(400, "missing \"hash\" param", ctx)
		}
		fileMd, err := srv.fs.GetFileMd(hash, true)
		if err != nil {
			if err == file_storage.ErrFileNotFound {
				return ctx.JSON(404, struct{}{})
			}
			return errors.Wrapf(err, "can not get file for hash \"%s\"", hash)
		}
		ctx.Response().Header().Set(echo.HeaderContentType, fileMd.ContentType)
		if err = ctx.Inline(fileMd.FilePath, fileMd.Name); err != nil {
			return errors.Wrapf(err, "can not retrieve file for hash \"%s\"", hash)
		}

		return nil
	})
	e.GET("/file/:hash/meta", func(ctx echo.Context) error {
		hash := ctx.Param("hash")
		if hash == "" {
			return setErrResponse(400, "missing \"hash\" param", ctx)
		}
		fileMd, err := srv.fs.GetFileMd(hash, false)
		if err != nil {
			if err == file_storage.ErrFileNotFound {
				return ctx.JSON(404, struct{}{})
			}
			return errors.Wrapf(err, "can not get file for hash \"%s\"", hash)
		}

		return ctx.JSON(http.StatusOK, fileMd)
	})
	e.DELETE("/file/:hash", func(ctx echo.Context) error {
		hash := ctx.Param("hash")
		if hash == "" {
			return setErrResponse(400, "missing \"hash\" param", ctx)
		}

		if err := srv.fs.DeleteFile(hash); err != nil {
			return errors.Wrapf(err, "can not delete file for hash \"%s\"", hash)
		}

		return ctx.JSON(200, struct{}{})
	})

	e.HTTPErrorHandler = func(e error, context echo.Context) {
		srv.log.Errorf("Internal error occurred: %+v", e)
		srv.e.DefaultHTTPErrorHandler(e, context)
	}
}

func Stop(srv *HttpServer) error {
	if err := srv.e.Shutdown(context.Background()); err != nil {
		return errors.Wrap(err, "error occurred during http server stop")
	}

	return nil
}

func setErrResponse(code int, errText string, ctx echo.Context) error {
	return ctx.JSON(code, map[string]interface{}{
		"message": errText,
	})
}
