package event_bus

const (
	EventAppStop      = "app:stop"
	EventHttpRequest  = "http:request"
	EventHttpResponse = "http:response"
)

type EvAppStop struct {
	err    error
	reason string
}

func (e *EvAppStop) Reason() string {
	return e.reason
}

func (e *EvAppStop) Err() error {
	return e.err
}

func NewEventAppStop(reason string, err error) *EvAppStop {
	return &EvAppStop{
		reason: reason,
		err:    err,
	}
}

func NewEventAppErrorStop(err error) *EvAppStop {
	return &EvAppStop{
		reason: "error",
		err:    err,
	}
}

type EvHttpRequest struct {
	// Any data
}

type EvHttpResponse struct {
	// Any data
}
