package config

type Config struct {
	ServiceName    string
	HttpHost       string
	HttpPort       uint
	HttpUser       string
	HttpPassword   string
	AppDir         string
	LogLevel       string
	FileStorageDir string
	RedisHost      string
	RedisPort      uint
	RedisPassword  string
	RedisDB        uint
}

func NewConfig() Config {
	return Config{}
}
