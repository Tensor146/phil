package logger

import (
	"github.com/sirupsen/logrus"

	"bitbucket.org/Tensor146/phil/app/internal/config"
)

const (
	LogLevelInfo = "info"
)

func NewLogger(cfg *config.Config) (*logrus.Logger, error) {
	log := logrus.New()
	logLevel, err := logrus.ParseLevel(cfg.LogLevel)
	if err != nil {
		return nil, err
	}
	log.SetLevel(logLevel)

	return log, nil
}
