package metadata_storage

import (
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"bitbucket.org/Tensor146/phil/app/internal/config"
	"bitbucket.org/Tensor146/phil/app/internal/models"
	"bitbucket.org/Tensor146/phil/app/pkg/helpers"
)

type MetadataStorage struct {
	red *redis.Client
	log *logrus.Logger
	cfg *config.Config
}

func NewMetadataStorage(log *logrus.Logger, cfg *config.Config) (*MetadataStorage, error) {
	if err := helpers.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	if err := helpers.NewUnexistingDependencyError("cfg", cfg); err != nil {
		return nil, err
	}
	ms := &MetadataStorage{
		log: log,
		cfg: cfg,
	}
	ms.red = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", cfg.RedisHost, cfg.RedisPort),
		Password: cfg.RedisPassword,
		DB:       int(cfg.RedisDB),
	})

	return ms, nil
}

func (ms *MetadataStorage) Serve() error {
	_, err := ms.red.Ping().Result()
	if err != nil {
		return errors.Wrap(err, "can not connect to redis")
	}

	return nil
}

func (ms *MetadataStorage) Stop() error {
	if err := ms.red.Close(); err != nil {
		return errors.Wrap(err, "can not close redis connection")
	}

	return nil
}

var NoMetadataFoundError = errors.New("metadata not found")

func (ms *MetadataStorage) GetByHash(hash string) (fileMd *models.FileMetadata, err error) {
	defer func() {
		ms.log.Debugf("metadata storage: get \"%s\": %+v %v", hash, fileMd, err)
	}()
	res, err := ms.red.HGetAll(ms.buildKey(hash)).Result()
	if err == redis.Nil || len(res) == 0 {
		err = NoMetadataFoundError
		return
	}
	if err != nil {
		err = errors.Wrap(err, "can not get file metadata: redis error")
		return
	}

	fileMd = &models.FileMetadata{}
	if err = models.UnmarshalFileMetadataFromRmap(models.RMap(res), fileMd); err != nil {
		err = errors.Wrap(err, "can not decode file metadata")
		return
	}
	if fileMd.Name == "" {
		err = errors.New("file metadata decoding error")
		return
	}

	return fileMd, nil
}

func (ms *MetadataStorage) IncDownloadsCount(hash string) (err error) {
	defer func() {
		ms.log.Debugf("metadata storage: inc downloads count for hash \"%s\"", hash, err)
	}()
	_, err = ms.red.HIncrBy(ms.buildKey(hash), "count_of_downloads", 1).Result()
	if err == redis.Nil {
		err = NoMetadataFoundError
		return
	}
	if err != nil {
		err = errors.Wrapf(err, "can not increment \"%s\": redis error", hash)
		return
	}

	return
}

func (ms *MetadataStorage) Save(fileMd models.FileMetadata) (err error) {
	defer func() {
		ms.log.Debugf("metadata storage: set \"%s\": %+v %v", fileMd.Hash, fileMd, err)
	}()
	var rmap *models.RMap
	rmap, err = models.MarshalFileMetadataIntoRmap(fileMd)
	if err != nil {
		err = errors.Wrap(err, "can not encode file metadata")
		return
	}
	fields := map[string]interface{}{}
	for key, value := range *rmap {
		fields[key] = value
	}
	redCmd := ms.red.HMSet(ms.buildKey(fileMd.Hash), fields)

	if _, err = redCmd.Result(); err != nil {
		err = errors.Wrap(redCmd.Err(), "can not save file metadata: redis error")
		return
	}

	return
}

func (ms *MetadataStorage) DeleteByHash(hash string) (err error) {
	defer func() {
		ms.log.Debugf("metadata storage: delete \"%s\": %v", hash, err)
	}()
	_, err = ms.red.Del(ms.buildKey(hash)).Result()
	if err != nil {
		return errors.Wrap(err, "redis error")
	}

	return nil
}

type rangeCallback func(fileMd *models.FileMetadata, n int64) bool
type rangeErrCallback func(err error, raw string) bool

func (ms *MetadataStorage) RangeAll(callback rangeCallback, errCallback rangeErrCallback) error {
	var cursor uint64
	var n int64
	var err error
	packSize := 100
	for {
		var keys []string
		keys, cursor, err = ms.red.Scan(cursor, "file_metadata:*", int64(packSize)).Result()

		if err != nil {
			return errors.Wrap(err, "redis scan error")
		}
		results, err := ms.red.MGet(keys...).Result()
		if err != nil {
			return errors.Wrap(err, "redis multi-get error")
		}
		var fileMd *models.FileMetadata
		for _, result := range results {
			resString, ok := result.(string)
			if !ok && !errCallback(errors.Errorf("Non-string redis entry: %+v", result), "") {
				return nil
			} else if err := json.Unmarshal([]byte(resString), fileMd); err != nil &&
				errCallback(errors.Wrapf(err, "Can not decode redis entry: %+v", result), resString) {
				return nil
			} else if !callback(fileMd, n) {
				return nil
			}
			n++
		}
		if cursor == 0 {
			break
		}
	}

	return nil
}

func (ms *MetadataStorage) buildKey(hash string) string {
	return fmt.Sprintf("file_metadata:%s", hash)
}
