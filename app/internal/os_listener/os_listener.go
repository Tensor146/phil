package os_listener

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/asaskevich/EventBus"

	"bitbucket.org/Tensor146/phil/app/internal/event_bus"
)

type OsListener struct {
	signals  chan os.Signal
	stop     chan struct{}
	bus      EventBus.Bus
	onceStop sync.Once
}

func NewOsListener(bus EventBus.Bus) *OsListener {
	return &OsListener{
		signals: make(chan os.Signal),
		stop:    make(chan struct{}),
	}
}

func Serve(listener *OsListener) {
	signal.Notify(listener.signals, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		select {
		case sig := <-listener.signals:
			listener.bus.Publish(event_bus.EventAppStop, event_bus.NewEventAppStop(fmt.Sprintf("OS sygnal: %s", sig), nil))
		case <-listener.stop:
			listener.onceStop.Do(func() {})
		}
	}()
}

func Stop(listener *OsListener) {
	listener.onceStop.Do(func() {
		listener.stop <- struct{}{}
	})
}
