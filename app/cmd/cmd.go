package cmd

import (
	"log"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/urfave/cli"

	"bitbucket.org/Tensor146/phil/app/internal/config"
	"bitbucket.org/Tensor146/phil/app/internal/logger"
)

const ServiceName = "phil"

const (
	flagAppDir = "app-dir"
)

const (
	flagValAppDirCurrent    = "%current"
	flagValAppDirWorkingDir = "%workdir"
)

func Run() {
	cfg := config.NewConfig()
	cfg.ServiceName = ServiceName

	cliApp := cli.NewApp()
	cliApp.Name = "phil"
	cliApp.Usage = "HTTP file storage service"
	cliApp.Version = "0.1.10"
	cliApp.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   flagAppDir,
			EnvVar: "PHIL_APP_DIR",
			Usage:  "Application work directory",
			Value:  flagValAppDirCurrent,
		},
		cli.StringFlag{
			Name:        "log-level",
			EnvVar:      "PHIL_LOG_LEVEL",
			Usage:       "Logging level",
			Value:       logger.LogLevelInfo,
			Destination: &cfg.LogLevel,
		},
	}

	cliApp.Before = func(context *cli.Context) error {
		var err error
		dir := context.String(flagAppDir)
		if dir == "" || dir == flagValAppDirCurrent {
			dir, err = filepath.Abs(filepath.Dir(os.Args[0]))
			if err != nil {
				return errors.Wrap(err, "can not get cliApp dir")
			}
		}
		if dir == flagValAppDirWorkingDir {
			dir, err = os.Getwd()
			if err != nil {
				return errors.Wrap(err, "can not get working dir")
			}
		}
		cfg.AppDir = dir

		return nil
	}

	cliApp.Commands = []cli.Command{
		buildCmdServe(&cfg),
	}

	err := cliApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
