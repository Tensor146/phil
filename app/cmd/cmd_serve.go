package cmd

import (
	"context"

	"github.com/asaskevich/EventBus"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"go.uber.org/fx"

	"bitbucket.org/Tensor146/phil/app/internal/config"
	"bitbucket.org/Tensor146/phil/app/internal/file_storage"
	"bitbucket.org/Tensor146/phil/app/internal/http"
	"bitbucket.org/Tensor146/phil/app/internal/metadata_storage"
)

func buildCmdServe(cfg *config.Config) cli.Command {
	return cli.Command{
		Name:  "serve",
		Usage: "Raises phil daemon",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:        "http-host",
				EnvVar:      "PHIL_HTTP_HOST",
				Usage:       "Http host address",
				Value:       "127.0.0.1",
				Destination: &cfg.HttpHost,
			},
			cli.UintFlag{
				Name:        "http-port",
				EnvVar:      "PHIL_HTTP_PORT",
				Usage:       "Http port",
				Value:       3000,
				Destination: &cfg.HttpPort,
			},
			cli.StringFlag{
				Name:        "http-username",
				EnvVar:      "PHIL_HTTP_USERNAME",
				Usage:       "Http auth access check: username",
				Value:       "",
				Destination: &cfg.HttpUser,
			},
			cli.StringFlag{
				Name:        "http-password",
				EnvVar:      "PHIL_HTTP_PASSWORD",
				Usage:       "Http auth access check: password",
				Value:       "",
				Destination: &cfg.HttpPassword,
			},
			cli.StringFlag{
				Name:        "redis-host",
				EnvVar:      "PHIL_REDIS_HOST",
				Usage:       "Redis host address",
				Value:       "127.0.0.1",
				Destination: &cfg.RedisHost,
			},
			cli.UintFlag{
				Name:        "redis-port",
				EnvVar:      "PHIL_REDIS_PORT",
				Usage:       "Redis port",
				Value:       6379,
				Destination: &cfg.RedisPort,
			},
			cli.StringFlag{
				Name:        "redis-password",
				EnvVar:      "PHIL_REDIS_PASSWORD",
				Usage:       "Redis password",
				Value:       "",
				Destination: &cfg.RedisPassword,
			},
			cli.UintFlag{
				Name:        "redis-db",
				EnvVar:      "PHIL_REDIS_DB",
				Usage:       "Redis database number (0-15)",
				Value:       0,
				Destination: &cfg.RedisDB,
			},
			cli.StringFlag{
				Name:        "file-storage-dir",
				EnvVar:      "PHIL_FILE_STORAGE_DIR",
				Usage:       "Dir to store saved files",
				Value:       "/tmp/phil/files",
				Destination: &cfg.FileStorageDir,
			},
		},
		Action: func(cliCtx *cli.Context) error {
			return RunApp(cfg,
				fx.Provide(
					func(lc fx.Lifecycle, fs *file_storage.FileStorage, bus EventBus.Bus, log *logrus.Logger, cfg *config.Config) (*http.HttpServer, error) {
						httpSrv, err := http.NewHttpServer(fs, bus, log, cfg)
						if err != nil {
							return nil, err
						}
						lc.Append(fx.Hook{
							OnStart: func(ctx context.Context) error {
								return http.Serve(httpSrv)
							},
							OnStop: func(ctx context.Context) error {
								return http.Stop(httpSrv)
							},
						})

						return httpSrv, nil
					},
					func(lc fx.Lifecycle, log *logrus.Logger, cfg *config.Config) (*metadata_storage.MetadataStorage, error) {
						ms, err := metadata_storage.NewMetadataStorage(log, cfg)
						if err != nil {
							return nil, err
						}
						lc.Append(fx.Hook{
							OnStart: func(ctx context.Context) error {
								return ms.Serve()
							},
							OnStop: func(ctx context.Context) error {
								return ms.Stop()
							},
						})

						return ms, nil
					},
					func(lc fx.Lifecycle, bus EventBus.Bus, ms *metadata_storage.MetadataStorage, log *logrus.Logger, cfg *config.Config) (*file_storage.FileStorage, error) {
						fs, err := file_storage.NewFileStorage(bus, ms, log, cfg)
						if err != nil {
							return nil, err
						}
						lc.Append(fx.Hook{
							OnStart: func(ctx context.Context) error {
								return fs.Serve()
							},
						})
						return fs, nil
					},
				),
				fx.Invoke(func(httpSrv *http.HttpServer, log *logrus.Logger) {
					log.Infof("Serving...")
				}))
		},
	}
}
