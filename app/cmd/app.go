package cmd

import (
	"context"

	"github.com/asaskevich/EventBus"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go.uber.org/fx"

	"bitbucket.org/Tensor146/phil/app/internal/config"
	"bitbucket.org/Tensor146/phil/app/internal/event_bus"
	"bitbucket.org/Tensor146/phil/app/internal/logger"
	//"bitbucket.org/Tensor146/phil/app/internal/os_listener"
)

func RunApp(cfg *config.Config, opts ...fx.Option) error {
	log, err := logger.NewLogger(cfg)
	if err != nil {
		return errors.Wrap(err, "can not instance the \"logger\" service")
	}
	eventBus := event_bus.NewEventBus(log)

	stop := make(chan struct{})

	opts = append([]fx.Option{
		fx.Provide(
			func() *config.Config {
				return cfg
			},
			func() *logrus.Logger {
				return log
			},
			func() EventBus.Bus {
				return eventBus
			},
			// FX is listening signals by itself, so it doesnt necessary for now.
			//func(lc fx.Lifecycle, bus EventBus.Bus) *os_listener.OsListener {
			//	osL := os_listener.NewOsListener(bus)
			//	lc.Append(fx.Hook{
			//		OnStart: func(i context.Context) error {
			//			os_listener.Serve(osL)
			//			return nil
			//		},
			//		OnStop: func(i context.Context) error {
			//			os_listener.Stop(osL)
			//			return nil
			//		},
			//	})
			//
			//	return osL
			//},
		),
		fx.Logger(&fxLoggerWrapper{log: log}),
	}, opts...)

	app := fx.New(
		opts...,
	)

	eventBus.SubscribeOnceAsync(event_bus.EventAppStop, func(event *event_bus.EvAppStop) {
		log.Infof("App stop, reason: %s", event.Reason())
		if err := event.Err(); err != nil {
			log.Errorf("Critical error occurred: %+v", event.Err())
		}
		if err := app.Stop(context.Background()); err != nil {
			log.Errorf("Error occurred during application stop: %+v", err)
		}
		stop <- struct{}{}
	})

	if err := app.Start(context.Background()); err != nil {
		return errors.Wrap(err, "can not start application")
	}

	<-stop

	return app.Err()
}

type fxLoggerWrapper struct {
	log *logrus.Logger
}

func (lW *fxLoggerWrapper) Printf(format string, args ...interface{}) {
	lW.log.Debugf(format, args...)
}
