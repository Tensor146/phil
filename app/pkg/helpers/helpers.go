package helpers

import (
	"github.com/pkg/errors"
)

func NewUnexistingDependencyError(depName string, depValue interface{}) error {
	if depValue == nil {
		return errors.Errorf("unexisting dependency: %s", depName)
	}

	return nil
}

func GoSafe(callback func()) (err error) {
	go func() {
		defer func() {
			if r := recover(); r != nil {
				var ok bool
				err, ok = r.(error)
				if ok {
					err = errors.Wrap(err, "Panic occurred")
				} else {
					err = errors.Errorf("Panic occurred (non-error arg): %+v", r)
				}
			}
		}()

		callback()
	}()

	return err
}
